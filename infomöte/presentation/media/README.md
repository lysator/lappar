Licenser för bilder
===================

### hatt.jpg

Copyright Nathaniel Mattsson

Använd med tillåtelse

### Server.jpg

Copyright Anton Haglund

Använd med tillåtelse

### Bar_utan_servrar.jpg

Copyright Anton Haglund

Använd med tillåtelse

### ättestupan.png

Copyright Johan Agrup 2020-2022

Använd utan tillstånd med Västgöta Nations i Linköpings välsignelse.

### bk4-link6.jpg
### `D21_1.jpg`

### helt-klart-hatt.png

Remix av [#ättestupan.png](ättestupan.png)

### lyslogo-eps-converted-to.pdf

### TEKS0048974.jpg

```
Description The first Swedish personal computer LYS-16
Date 	    16 March 2000, 19:31:41
Source 	    https://digitaltmuseum.se/021026362044/mikrodator
Author 	    Fredrik Pihl
```

https://commons.wikimedia.org/wiki/File:TEKS0048974.jpg

Creative Commons Attribution-Share Alike 4.0 International

### Termi-LYS_terminal.jpg
```
Description This terminal was used to get terminal input on a TV
            screen for the LYS-16 personal computer in the 1970ies.
Date 	    16 March 2000, 20:33:14
Source 	    https://digitaltmuseum.se/021026362045/teminal
Author 	    Fredrik Pihl
```

Creative Commons Attribution-Share Alike 4.0 International

### tilde-2023-09-05.jpg

Copyright Hugo Hörnquist 2023

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

### uni-antenn.jpg
